FROM python:3.9-alpine3.12
COPY . .
RUN pip install -r requirements.txt
WORKDIR /demo_cstech 
CMD ["python", "manage.py", "runserver", "0:8000"]
